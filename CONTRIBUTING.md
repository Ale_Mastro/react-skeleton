# Contributing to [Project Name]

Thank you for your interest in contributing to React-Skeleton! We welcome contributions from the community and appreciate your effort in making this project better.

## How to Contribute

To contribute to React-Skeleton, please follow these steps:

1. Fork the repository and create a new branch for your contribution.
2. Make the necessary changes or additions to the codebase.
3. Write tests to ensure the correctness of your changes.
4. Commit your changes and push them to your forked repository.
5. Submit a pull request to the `main` branch of the original repository.

## Pull Request Guidelines

When submitting a pull request, please ensure the following:

1. Provide a clear and descriptive title for your pull request.
2. Include a detailed description of the changes you have made.
3. Ensure that your code follows the project's coding conventions and style guidelines.
4. If your pull request addresses an issue, reference the issue number in the description using the syntax `Fixes #issue_number`.
5. Include any relevant screenshots, GIFs, or other media to demonstrate the changes if applicable.

## Code Style

Please follow the existing code style and conventions used in the project. This helps maintain consistency across the codebase and makes it easier for others to understand and contribute to the project.

## Testing

Before submitting a pull request, please ensure that your changes pass all the tests and write additional tests if necessary. This helps maintain the quality and stability of the project.

## Reporting Issues

If you encounter any bugs, issues, or have suggestions for improvement, please open an issue in the [Issue Tracker](https://gitlab.com/Ale_Mastro/react-skeleton/issues). Provide a clear and concise description of the problem or suggestion, including any relevant information and steps to reproduce the issue if applicable.

## License

By contributing to React-Skeleton, you agree that your contributions will be licensed under the [LICENSE](https://gitlab.com/Ale_Mastro/react-skeleton/blob/main/LICENSE) of the project.

---

We appreciate your time and effort in contributing to React-Skeleton. Your contributions help make this project better for everyone. Thank you!
