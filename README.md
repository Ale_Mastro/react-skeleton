# React Skeleton

This is a React Skeleton project that serves as a starting point for building React applications. It includes several essential features and libraries to accelerate development.

## Features

- **Jest** for testing
- **Storybook** for documenting components and deploying to GitLab Pages
- **React** Context for managing application-wide state
- **Redux** for state management
- **Sass** for styling
- **Axios** for handling API requests

## Prerequisites

Before getting started, ensure that you have the following software installed on your machine:

- Node.js (version >= 12.0.0)
- npm (version >= 6.0.0)

## Installation

1. Clone the repository:

```bash
git clone https://gitlab.com/Ale_Mastro/react-skeleton.git
```
2. Change to the project directory:

```bash
cd react-skeleton
```
3. Install dependencies:
```bash
npm install
```

## Usage
### Development
To start the development server and run the application locally, use the following command:
```bash
npm start
```
This will start the development server and open the application in your default browser. Any changes you make to the source code will automatically reload the application.

### Development
To run the Jest tests, use the following command:

```bash
npm test
```
This will execute the test suite and provide feedback on the test results.

### Building
To build the production-ready optimized bundle of your application, use the following command:

```bash
npm run build
```
The production build will be generated in the <b>`build`</b> directory.

#### Storybook Documentation
To view the Storybook documentation locally, use the following command:

```bash
npm run storybook
```
This will start the Storybook server and open it in your default browser. You can explore the various components and their documentation.



## Deploying Storybook to GitLab Pages
The project includes a GitLab CI configuration `(.gitlab-ci.yml)` that enables deploying the Storybook documentation to GitLab Pages. Here's an explanation of the configuration:
```bash
stages:
  - test
  - deploy

# This is an example of CI that aims to deploy the static Storybook inside GitLab Pages
# Important: always set a Node.js version to avoid errors
# Cached parameters
cache:
  paths:
    - node_modules/

pages:
  image: node:latest
  stage: deploy
  # Initialization of the dependencies and moving the static site made by build-storybook
  # inside the public folder (point at which GitLab Pages have access)
  script:
    - npm install
    - npm run build-storybook
    - rm -r public/
    - mkdir public
    - cp -r storybook-static/* public/
    - cd public
  artifacts:
    paths:
      - public
  # This CI job will run only on the 'main' branch
  only:
    - main
  when: manual

unit-test:
  image: node:latest
  stage: test
  before_script:
    - npm install
  script:
    - npm run test:ci
  coverage: /All files[^|]*\|[^|]*\s+([\d\.]+)/
  artifacts:
    paths:
      - coverage/
    when: always
    reports:
      junit:
        - junit.xml

```
The GitLab CI configuration consists of two stages: `test` and `deploy`.

1. The `unit-test` job runs the Jest tests and generates a coverage report. The coverage report is stored in the `coverage/` directory and is available as an artifact. This job will always run, regardless of the branch.
2. The `pages` job handles the deployment of the Storybook documentation to GitLab Pages. It uses the Node.js image, installs the dependencies, builds the Storybook static site, moves the generated files to the `public/`directory, and uploads the` public/` directory as an artifact. This job is triggered manually and only runs on the main branch.
Please note that you may need to configure your GitLab Pages settings and environment variables for the deployment to work correctly.

## Contributing

If you'd like to contribute to this project, please follow the guidelines in CONTRIBUTING.md.

## License

This project is licensed under the MIT License.

