import { createSlice } from '@reduxjs/toolkit';

const initCounter = { counter: 0, showCounter: true };
// We prepare slice of state
const counterSlice = createSlice({
  name: 'counter',
  initialState: initCounter,
  // Reducer functions -> standard js function that gets the old and the new value
  reducers: {
    increment(state, action)  {
      state.counter += action.payload.amount;
    },
    decrement(state, action)  {
      state.counter -= action.payload.amount;
    },
    toggle(state)  {
      state.showCounter =  !state.showCounter;
    },
  }
});
export const counterActions = counterSlice.actions;

export default counterSlice.reducer;