import { createSlice } from '@reduxjs/toolkit';

const initAuth = { isAuthenticated: false };

const authSlice = createSlice({
  name: 'auth',
  initialState: initAuth,
  // Reducer functions -> standard js function that gets the old and the new value
  reducers: {
    login(state)  {
      state.isAuthenticated = true;
    },
    logout(state)  {
      state.isAuthenticated = false;
    }
  }
});
export const authActions = authSlice.actions;

export default authSlice.reducer;