import { useSelector, useDispatch } from 'react-redux';
import { authActions } from '../store/slices/auth';

export default function Auth() {
    const dispatch = useDispatch();

    const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);

  const login = () => {
    dispatch(authActions.login());
  };

  return (
    <div>
        { isAuthenticated ? <div>You are logged</div> :  <div>You are not logged</div>}
      <button onClick={login}>Login</button>
    </div>
  );
}
