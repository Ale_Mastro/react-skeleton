import logo from '../logo.svg';
import './Header.scss'

import { useDispatch } from 'react-redux';
import { authActions } from '../store/slices/auth';

import { Link } from 'react-router-dom';

export default function Header() {
    const dispatch = useDispatch();
    // const isAuthenticated = useSelector(state => state.auth.isAuthenticated);

    const logout = () => {
        dispatch(authActions.logout());
    }; 

    return (
    <header className="App-header">
    <div className="React-Infos">
      <img src={logo} className="App-logo" alt="logo" />
      <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
        Learn React
      </a>
    </div>
    <p>This is a React Skeleton to start your project</p>
    <Link to='/'>Home</Link>
    <Link to='/router'>Router</Link>
    <Link to='/error'>Error Page</Link>
    <button onClick={logout}>Logout</button>
  </header>
  )
}
