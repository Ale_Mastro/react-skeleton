import './ProjectInfo.scss';

export default function ProjectInfo() {
  return (
    <div className='Project-container'>
        This project is made to give you a starting point for your React Project including all the main libraries which you have to include in your project.
        <div className='list'>
            <p>React-Redux</p>
            <p>Redux Toolkit</p>
            <p>React Router</p>
        </div>
    </div>
  )
}
