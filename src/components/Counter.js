import './Counter.scss';
// Allows us to select a part of the state in store
import { useSelector, useDispatch } from 'react-redux';
import { counterActions } from '../store/slices/counter';
function Counter() {
    const dispatch = useDispatch();
  // It makes also the subscription
    const counter = useSelector(state => state.counter.counter); // Split the state in order to get a bunch of the state
    const showCounter = useSelector(state => state.counter.showCounter); // Split the state in order to get a bunch of the state

    const increment = () => {
        dispatch(counterActions.increment({ amount: 1 }));
    };
    const decrement = () => {
        dispatch(counterActions.decrement({ amount: 1 }));
    };
    const toggle = () => {
        dispatch(counterActions.toggle());
    };

  return (
    <main>
      <h1>Redux Counter</h1>
      <div>{showCounter && counter}</div>
      <div>
        <button onClick={increment}>Increment</button>
        <button onClick={decrement}>Decrement</button>
      </div>
      <button onClick={toggle}>Hide Counter</button>
    </main>
  );
}

export default Counter;
