import './App.scss';
import { Fragment } from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import RouterPage from './pages/RouterPage';
import HomePage from './pages/HomePage';
import Root from './pages/Root';
import ErrorPage from './pages/ErrorPage';

const router = createBrowserRouter([
  { path: '/', element: <Root/> ,
  errorElement: <ErrorPage />,
  children:[
    { path: '/', element: <HomePage/> },
    { path: '/router', element: <RouterPage/> },
    {}
  ]}
]);
function App() {
  return (
    <div className="App">
    <Fragment>
      <RouterProvider router={router} />
    </Fragment>
  </div>
  );
}

export default App;
