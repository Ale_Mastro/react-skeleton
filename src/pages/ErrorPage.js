import Header from '../components/Header';
import './ErrorPage.scss';

export default function ErrorPage() {
  return (
    <div className='container'>
      <Header />
    <div className='row'>
        <img src='./working.gif'/>
    </div>
    </div>
  )
}
