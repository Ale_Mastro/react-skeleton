import '../App.scss';
import Counter from '../components/Counter';
import Header from '../components/Header';
import Auth from '../components/Auth';
import { Fragment } from 'react';
import ProjectInfo from '../components/ProjectInfo';



export default function HomePage() {
    return (

          <div class="App-Content">
            <ProjectInfo />
            <div>
              <Auth />
              <Counter />
            </div>
          </div>

    )
  }

